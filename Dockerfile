FROM alpine:latest
MAINTAINER Maikel Dollé <m.dolle@haarlem105.nl>

RUN addgroup -S icecast && \
    adduser -S icecast

RUN apk add --update \
        icecast \
        mailcap && \
    rm -rf /var/cache/apk/*

EXPOSE 8000
VOLUME ["/var/log/icecast"]

CMD icecast -c /icecast.xml
